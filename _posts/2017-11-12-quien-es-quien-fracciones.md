---
layout: post
title: "Quién es quién | Fracciones"
description: "Estas actividad está diseñado para generar conversaciones con un amplio vocabulario sobre fracciones y relaciones entre partes y todo"
date:   2018-12-06 17:46:41 -0300
categories: racionales blog
by: 'Equipo Aceleración'
icon: 'help-circle'
questions:
  - question: 'Fracciones, partes del entero y equivalencias'
    answer: 'El vocabulario clave que puede aparecer en las preguntas de los estudiantes incluye: sombreado, no sombreado, fracción, parte, entero, numerador, denominador, simplificado y equivalente / igual a. 

En las primeras rondas del juego, los estudiantes pueden notar las características numéricas de la lista anterior, aunque no usen esas palabras para describirlas. Ahí es donde puede intervenir. 

Después de que la mayoría de los estudiantes hayan jugado 2-3 juegos, considere tomarse un breve descanso para discutir la estrategia, resaltar preguntas efectivas y alentar a los estudiantes a usar un lenguaje académico cada vez más preciso. Luego, pídales que jueguen varios juegos más y que pongan en práctica ese lenguaje preciso.'
    image: "shadeBoxes.png"
    linkurl: "https://teacher.desmos.com/polygraph/custom/5beb6afa1970457ef71d634b"
    linkdesc: "Link | Actividad en Desmos"
  - question: 'Fracciones, cuartos, octavos, tercios y sextos'
    answer: 'El vocabulario clave que puede aparecer en las preguntas de los estudiantes incluye: sombreado, no sombreado, fracción, parte, entero, numerador, denominador, simplificado y equivalente / igual a. 

En las primeras rondas del juego, los estudiantes pueden notar las características numéricas de la lista anterior, aunque no usen esas palabras para describirlas. Ahí es donde puede intervenir. 

Después de que la mayoría de los estudiantes hayan jugado 2-3 juegos, considere tomarse un breve descanso para discutir la estrategia, resaltar preguntas efectivas y alentar a los estudiantes a usar un lenguaje académico cada vez más preciso. Luego, pídales que jueguen varios juegos más y que pongan en práctica ese lenguaje preciso.'
    image: "shadeRectangles.png"
    linkurl: "https://teacher.desmos.com/polygraph/custom/5be2ddca50a22031cc60a564"
    linkdesc: "Link | Actividad en Desmos"
---
