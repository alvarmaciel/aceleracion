---
layout: post
title: "Multiplicaciones de números mayores "
description: "Actividades del cuadernillo Trayectorias 2."
date:   2017-06-12 17:46:41 -0300
categories: inicio blog
by: 'Equipo Aceleración'
icon: 'help-circle'
questions:
  - question: 'Multiplicaciones de números mayores '
    answer: ' Acá va una descripción'
    image: "1.gif"
    linkurl: "https://teacher.desmos.com/activitybuilder/custom/5adfac3a1a047c1c30f20870"
    linkdesc: "Actividad en Desmos"
  - question: 'Desarmar números para multipliccar'
    answer: 'Acá va una descripción'
    image: "2.gif"
    linkurl: "https://teacher.desmos.com/activitybuilder/custom/5aec4d59dbc57309f1a885ba"
    linkdesc: "Actividad en Desmos"
  - question: 'Usar multiplicaciones para resolver otras'
    answer: 'Acá va una descripción'
    image: "3.gif"
    linkurl: "https://teacher.desmos.com/activitybuilder/custom/5b02c38d479dde0ac6b0ff17"
    linkdesc: "Actividad en desmos"
---
