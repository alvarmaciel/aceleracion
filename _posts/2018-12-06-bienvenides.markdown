---
layout: post
title: "Bienvenida"
description: "Sobre este sitio"
date:   2018-12-06 17:46:41 -0300
categories: inicio blog
by: 'Equipo Aceleracion'
icon: 'loader'
---
Esta es una maqueta para el [Programa de Aceleración de la Ciudad de Buenos Aires](http://programaaceleracion.blogspot.com/)

El sitio está diseñado para contener las actividades, secuencias y proyectos armados en [desmos.com](https://desmos.com)

Desmos es una plataforma pensada para facilitar el aprendizaje de las matemáticas a través del uso de tecnologías digitales.




<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/dan_meyer_math_curriculum_makeover" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>


